export default {

    methods: {
        send() {
            let index = this.getIndexQuestion()


            if (index != -1 || index < this.qnt_questions) {
                this.dispatchData(index)
                return this.$store.dispatch('RESET_TIME')
            }

            return this.finished()
        },

        dispatchData(index) {
            let question

            if (index == this.qnt_questions) {
                question = null
            } else {
                question = this.questions[index + 1]
            }

            this.$store.dispatch('RESOLVE_QUESTION', {
                next_question: question,
                answered_question: this.getAnsweredQuestion(),
            })

            this.$store.commit('TOGGLE_TRANSITION', true)
            setTimeout(() => {
                this.$store.commit('TOGGLE_TRANSITION', false)
            }, 100)

            if (null === question) {
                this.finished()
            }
        },

        getIndexQuestion() {
            return this.questions.findIndex(qst => qst.answer === this.active_question.answer
                                                && qst.path === this.active_question.path)
        },

        getAnsweredQuestion() {
            return {
                ...this.active_question,
                assert: this.answer_selected === null ? null : this.active_question.answer == this.answer_selected
            }
        },

        finished() {
            this.$store.dispatch('FINISH')
            return this.$router.push('/finalizado')
        }
    }
}