export function choice(arr) {
    let new_array = []

    let selected_index = []

    while (new_array.length < arr.length) {
        let index = Math.floor(Math.random() * arr.length)

        if (selected_index.indexOf(index) == -1) {
            new_array.push(arr[index])
            selected_index.push(index)
        }
    }

    return new_array
}