const webpack = require('webpack');
const root = require('./.root_dir.js');
const ROOT_DIR = root.ROOT_DIR;

module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'Infraero',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Nuxt.js project' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },

  router: {
    base: '/' + ROOT_DIR + '/',
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Build configuration
  */
  modules: ['@nuxtjs/axios'],

  axios: {
    // proxyHeaders: false
  },

  css: ['bootstrap/dist/css/bootstrap.css'],

  plugins: ['~plugins/bootstrap.js'],

  build: {
    vendor: ['jquery', 'bootstrap'],

    plugins: [
      new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery',
        'window.jQuery': 'jquery',
      }),
    ],
    /*
    ** Run ESLint on save
    */
    extend(config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
        });
      }
    },
  },
};
