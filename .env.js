export const IMAGE_LOGO = 'images/logo.png';
export const MONITOR_IMG = 'images/monitor_op1.png';
export const BACKGROUND_IMG = 'images/bg.jpg';
export const ANSWER_AREA = 'images/answer-area.png';
export const BTN_AREA = 'images/btn-area.png';
export const BTN_1 = 'images/btn1.png';
export const BTN_2 = 'images/btn2.png';
export const BTN_3 = 'images/btn3.png';
export const BTN_4 = 'images/btn4.png';
export const BTN_NEXT = 'images/c_right.png';
export const BTN_BACK = 'images/c_left.png';
export const API_URL = 'http://localhost:8000';

export const FIREBASE_API_KEY = 'AIzaSyDrgDklZ368iTMLZbOYDILEu6CAVmbYlWQ';
export const FIREBASE_AUTH_DOMAIN = 'infs-38572.firebaseapp.com';
export const FIREBASE_DB_URL = 'https://infs-38572.firebaseio.com';
export const FIREBASE_STORAGE_BKT = 'infs-38572.appspot.com';
export const FIREBASE_PROJECT_ID = 'infs-38572';
