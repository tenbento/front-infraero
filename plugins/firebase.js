import firebase from 'firebase'
import {FIREBASE_API_KEY,
        FIREBASE_AUTH_DOMAIN,
        FIREBASE_DB_URL,
        FIREBASE_STORAGE_BKT,
        FIREBASE_PROJECT_ID} from '~/.env.js'

if (!firebase.apps.length) {
    const config = {
        apiKey: FIREBASE_API_KEY,
        authDomain: FIREBASE_AUTH_DOMAIN,
        databaseURL: FIREBASE_DB_URL,
        storageBucket: FIREBASE_STORAGE_BKT,
        projectId: FIREBASE_PROJECT_ID,
    }

    firebase.initializeApp(config)
}

export default firebase