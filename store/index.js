import Vuex from 'vuex'
import {mutations} from '~/store/mutations'
import {actions} from '~/store/actions'

export const state = {
    user: {},
    questions: [],
    qnt_questions: 0,
    answered_questions: [],
    active_question: {},
    time_to_resolve: null,
    type_image: "original",
    answer_selected: null,
    reset_time: true,
    transition_control: false,
}



const createStore = () => {
    return new Vuex.Store({
        state,
        mutations,
        actions
    })
}

export default createStore