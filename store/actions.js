import { API_URL } from "~/.env";
import axios from 'axios';
import firebase from '~/plugins/firebase'

export const actions = {
    REGISTER: ({commit}, payload) => {
        return firebase.auth().createUserWithEmailAndPassword(payload.email, payload.password)
            .then(({user}) => {
                return user.updateProfile({
                    displayName: payload.name
                }).then(() => {
                    commit('SET_USER', {
                        name: user.displayName,
                        email: user.email,
                        uid: user.uid,
                    })
                    return {
                        status: true
                    }
                })
            })
            .catch(error => ({
                status: false,
                code: error.code,
                message: error.message,
            }))
    },

    LOGIN: ({commit}, payload) => {
        return firebase.auth().signInWithEmailAndPassword(payload.email, payload.password)
            .then(({user}) => {
                commit('SET_USER', {
                    name: user.displayName,
                    email: user.email,
                    uid: user.uid,
                })
                return {
                    status: true
                }
            })
            .catch(error => ({
                status: false,
                code: error.code,
                message: error.message,
            }))
    },

    RESOLVE_QUESTION: ({commit}, payload) => {
        commit('SET_TYPE_IMAGE', 'original')
        commit('SET_ANSWER_SELECTED', null)
        commit('PUSH_ANSWERED_QUESTION', payload.answered_question)
        if (payload.next_question) {
            commit('SET_ACTIVE_QUESTION', payload.next_question)
        }
    },

    FINISH: ({commit}) => {
        commit('SET_QUESTIONS_TO_EMPTY')
    },

    RESET_TIME: ({commit}) => {
        commit('TOGGLE_RESET_TIME', false)
        setTimeout(() => {
            commit('TOGGLE_RESET_TIME', true)
        })
    },

    SEND: ({commit}, form) => {
        firebase.database().ref('metrics').push({
            user_uid: form.user_uid,
            user_name: form.user_name,
            user_email: form.user_email,
            total_questions: form.total_questions,
            right_questions: form.right_questions,
            wrong_questions: form.wrong_questions,
            blank_questions: form.blank_questions,
            time_to_resolve: form.time_to_resolve,
        })
    }
}