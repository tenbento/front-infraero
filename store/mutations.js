import all_asserts from '~/map_images.js'
import {choice} from '~/utils/choice'

export const mutations = {
    SET_USER: (state, user) => {
        state.user = user
    },

    SET_QUESTIONS_TO_EMPTY: (state) => {
        state.questions = []
    },

    ARRAY_CHOICE: (state) => {
        state.questions = choice(all_asserts)
        state.active_question = state.questions[0]
        state.qnt_questions = state.questions.length
    },

    SET_ACTIVE_QUESTION: (state, question) => {
        state.active_question = question
    },

    SET_TYPE_IMAGE: (state, type) => {
        state.type_image = type
    },

    SET_ANSWER_SELECTED: (state, answer) => {
        state.answer_selected = answer
    },

    SET_ANSWERED_QUESTIONS_TO_EMPTY: (state) => {
        state.answered_questions = []
    },

    SET_TIME_TO_RESOLVE: (state, time) => {
        state.time_to_resolve = time
    },

    TOGGLE_RESET_TIME: (state, payload) => {
        state.reset_time = payload
    },

    PUSH_ANSWERED_QUESTION: (state, question) => {
        state.answered_questions.push(question)
    },

    TOGGLE_TRANSITION: (state, status) => {
        state.transition_control = status
    }
}