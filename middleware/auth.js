export default function ({store, redirect}) {
    if (!store.state.user.uid) {
        return redirect('/login')
    }
}